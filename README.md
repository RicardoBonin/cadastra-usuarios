## Gerenciado de usuários

Faça o clone do repositório do projeto: git clone git@bitbucket.org:RicardoBonin/cadastra-usuarios.git

Faça o clone do servidor: git clone git@bitbucket.org:RicardoBonin/banco-de-dado-json.git


### `Start no projeto e no servidor`

No seu shell vá até o seu repositorio do projeto e de o comando: npm install
Para dar o start no projeto de o comando: yarn start
Abra o Projeto [http://localhost:3000](http://localhost:3000)

Feito isso. Abra um nova aba do shell e vá até o repositório do servidor.
Instale o npm: npm install
Para rodar o servidor de o comando: npm run start:backend
Abra o Servidor [http://localhost:3001](http://localhost:3001)


### `Login`

Para fazer o login acesse o bando de bado em escolha um usuário:[http://localhost:3001/usuarios](http://localhost:3001/usuarios)

Para testar o Gerenciador como Administrador escolha no banco de dados um usuário com o campo tipoUsuario: "Administrador".

Para testar o Gerenciador como Usuário Padrão escolha no banco de dados um usuário com o campo tipoUsuario: "Usuário Padrão".

Pege o e-mail e a senha do usuário e faça o login para ter acesso ao sistema.
