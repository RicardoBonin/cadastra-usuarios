import { useReducer, useEffect} from 'react'
import axios from 'axios'

const initialState = {
    loading: true,
    data: {}
}

const reducer = (state, action) => {
    if(action.type === 'REQUEST'){
      return{
      ...state,
      loading: true
      }
    }
  
    if(action.type === 'SUCCESS') {
      return {
        ...state,
        loading: false,
        data: action.data
      }
    }
    //Manipular meu estado
    return state
}

const init = baseURL =>{
    const useGet = resource => {
        const [data, dispatch] = useReducer(reducer,initialState)
        const carregar = async () =>{
          dispatch({ type: 'REQUEST'})
          const res = await axios.get(baseURL + resource)
          dispatch({ type: 'SUCCESS', data: res.data})
        }
          useEffect(() =>{
            carregar()
          }, [resource])
          return {
            ...data,
            refetch: carregar
          }
    }
    

    const usePost = resource => {
        const [data, dispatch] = useReducer(reducer,initialState)
        
        const post = async(data) => {
          dispatch({ type: 'REQUEST'})
          const res = await axios.post(baseURL + resource, data)
            dispatch({
                type: 'SUCCESS',
                data: res.data
            })
        }
          return [data, post]
    }

    const useDelete = resource => {
        const [data, dispatch] = useReducer(reducer, initialState)
        const remove = async(resource) => {
            dispatch({ type: 'REQUEST'})
            const res = await axios.delete(baseURL + resource, data)
                dispatch({
                    type: 'SUCCESS',
                    data: res.data
                })
        }
        return [data, remove]
    }
    
    const usePut = resource => {
      const [data, dispatch] = useReducer(reducer,initialState)
      const edite = data => {
        dispatch({type: 'REQUEST'})
        axios
          .put(baseURL + resource, data)
          .then(res => {
          dispatch({
            type: 'SUCCESS',
            data: res.data
          })
        })
      }
      return [data, edite]
    }
    return {
        useGet,
        usePost,
        usePut,
        useDelete
    }
}




export default init