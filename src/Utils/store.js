import React, { createContext, useReducer} from 'react'

const initialState = {
    usuarioLogado: {
      id: 0,
      nome: '',
      sobrenome: '',
      tipoUsuario: 'Administrador',
      email: '',
      ativo: false,
      logado: false
    }
}

  const store = createContext(initialState)
  const { Provider } = store
  
  const StateProvider = ({ children }) => {
    const [state, dispatch] = useReducer((state, action) => {
      switch (action.type) {
        case 'USER_LOGIN':
          return {
            ...state,
            usuarioLogado: {
              id: action.data.id,
              nome: action.data.nome,
              sobrenome: action.data.sobrenome,
              tipoUsuario: action.data.tipoUsuario,
              email: action.data.email,
              ativo: action.data.ativo,
              logado: true
            },
            loginError: ''
          }
        case 'LOGIN_ERROR':
          return {
            ...state,
            usuarioLogado: initialState,
            loginError:
              'Ops! Email ou senha errados ou você não está cadastrado.'
          }
          case 'LOGOUT':
            return {
              ...state,
              usuarioLogado: initialState
              
            }
        default:
          throw new Error()
      }
    }, initialState)
  
    return <Provider value={{ state, dispatch }}>{children}</Provider>
  }
  
  export { store, StateProvider }
