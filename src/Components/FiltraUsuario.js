import React, {useState, useContext} from 'react'
import { Link, Redirect } from 'react-router-dom'
import Rest from '../Utils/rest'
import { store } from '../Utils/store'

const baseUrl = 'http://localhost:3001/'
const { useGet, useDelete } = Rest(baseUrl)

const FiltraUsuario = () => {
    const data = useGet('usuarios')
    const { state } =  useContext(store)
    const [user, setUser] = useState('')
    const [usuarioFiltrado, setUsuarioFiltrado] = useState({})
    const [success, setSuccess] = useState(false)
    const [deleteData, remove] = useDelete()
    const deleteUsuario = id => {
        remove('usuarios/' + id)
        setSuccess(true)
    }
    if(success){
        return  <Redirect to='/' />
    }

    const usuarioFilter = evt => {
      setUser(evt.target.value)
    }
    const filterItems = () => {
        setUsuarioFiltrado(data.data.filter(obj =>  obj.nome.toLowerCase() === user.toLowerCase()))
    }
    return (
      <div>
        <div className='input-group mb-3 mt-5'>
          <input type='text' className='form-control' placeholder='Digite um nome' aria-label="Recipient's username" aria-describedby='basic-addon2' onChange={usuarioFilter} />
        
          <div className='input-group-append'>
           <button className='btn btn-outline-secondary' type='button' onClick={filterItems}>Buscar</button>
          </div>
        </div>
          {usuarioFiltrado.length > 0 &&
            <table  className='table table-dark rounded'>
              <thead>
                <tr>
                  <th scope='col'>Nome</th>
                  <th scope='col'>Sobre Nome</th>
                  <th scope='col'>E-mail</th>
                  <th scope='col'>Tipo de Usuário</th>
                  <th scope='col'>Status</th>
                  {state.usuarioLogado.tipoUsuario === 'Administrador' &&
                    <th scope='col'>Ações</th>
                  }
                </tr> 
              </thead>
              <tbody>
                {Object
                  .keys(usuarioFiltrado)
                  .map(user => {
                    return(
                      <tr key={user}>
                        <td>{usuarioFiltrado[user].nome}</td>
                        <td>{usuarioFiltrado[user].sobrenome}</td>
                        <td>{usuarioFiltrado[user].email}</td>
                        <td>{usuarioFiltrado[user].tipoUsuario}</td>
                        <td>{data.data[user].ativo?(<p>true</p>):(<p>false</p>)}</td>
                        {state.usuarioLogado.tipoUsuario === 'Administrador' &&
                        <td>
                            <button className='btn btn-danger mr-2' onClick={ () => deleteUsuario(data.data[user].id)}>Delete</button>
                            <Link className='btn btn-warning' to={'/usuarios/' + data.data[user].id}>Editar</Link>
                        </td>
                        }
                      </tr>
                    )
                })
                }
              </tbody>
            </table>
          }
          {usuarioFiltrado.length === 0 &&
            <div className='alert alert-warning' role='alert'>
              ['ERRO'] Por Favor, preencha o campo de Busca!
            </div>
          }
      </div>
    )
  }
export default FiltraUsuario