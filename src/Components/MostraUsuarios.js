import React, {useState, useContext} from 'react'
import { Link, Redirect } from 'react-router-dom'
import Rest from '../Utils/rest'
import { store } from '../Utils/store'
import FiltraUsuario from './FiltraUsuario'

const baseUrl = 'http://localhost:3001/'

const { useGet, useDelete } = Rest(baseUrl)

const MostraUsuarios = () => {
  const data = useGet('usuarios')
  const { state } =  useContext(store)
  const [deleteData, remove] = useDelete()
  const deleteUsuario = async(id) => {
    await remove('usuarios/' + id)
    data.refetch()
  }
 
    return state.usuarioLogado.logado ? (
      
      <div>
        <FiltraUsuario />
        <div>
          {Object.keys(data).length>0 &&
            <table className='table table-dark rounded'>
              <thead>
                <tr>
                  <th scope='col'>Nome</th>
                  <th scope='col'>Sobre Nome</th>
                  <th scope='col'>E-mail</th>
                  <th scope='col'>Tipo de Usuário</th>
                  <th scope='col'>Status</th>
                  {state.usuarioLogado.tipoUsuario === 'Administrador' &&
                  <th scope='col'>Ações</th>
                  }
                </tr> 
              </thead>
              <tbody>
                {
                  Object
                  .keys(data.data)
                  .map(user => {
                    return (
                      <tr key={user}>
                        <td>{data.data[user].nome}</td>
                        <td>{data.data[user].sobrenome}</td>
                        <td>{data.data[user].email}</td>
                        <td>{data.data[user].tipoUsuario}</td>
                        <td>{data.data[user].ativo?(<p>Ativo</p>):(<p>Inativo</p>)}</td>
                        {state.usuarioLogado.tipoUsuario === 'Administrador' &&
                        <td>
                          <button className='btn btn-danger mr-2' onClick={ () => deleteUsuario(data.data[user].id)}>Delete</button>
                          <Link className='btn btn-warning' to={'/usuarios/' + data.data[user].id}>Editar</Link>
                        </td>
                        }
                      </tr>
                    )
                  })
                }
              </tbody>
            </table>
          }
          </div>
          <div>
            {state.usuarioLogado.tipoUsuario === 'Administrador' &&
              <Link className='btn btn-primary' to='/usuario/novo'>Adicionar Novo Usuário</Link>
            }
          </div>
      </div>
      
    ) : (
        <div>
          <div className='alert alert-warning' role='alert'>
            Acesso Restrito. Faça o seu login ou entre em contado com o administrador.
          </div>
          
          <Link className='btn btn-primary' to='/login'>Fazer Login</Link>
        </div>
        )
}
export default MostraUsuarios