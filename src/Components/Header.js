import React, { useContext, useEffect } from 'react'
import { Link }  from 'react-router-dom'
import  { store } from '../Utils/store'

const Header = () => {
  const { state, dispatch } = useContext(store)
  console.log(state)
  const logout = () => {
    
    dispatch({ type: 'LOGOUT' })

  }
    return (
      
      <nav className='navbar navbar-light bg-light'>
      <div className='container'>
        <Link className='navbar-brand' to='/'>{`Olá ${state.usuarioLogado.nome}. Seja Bem vindo ao Gerenciador de Usuários.`}</Link>
       <ul className='navbar-nav  float-left'>
          <li className='nav-item'>
            <button type='button' className='btn nav-link' onClick={logout}>Sair</button>
          </li>
        </ul>
        
      </div>
      </nav>
    )
}

  export default Header