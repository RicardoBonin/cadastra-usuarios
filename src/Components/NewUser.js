import React, { useState, useContext } from 'react'
import { Redirect, Link } from 'react-router-dom'
import Rest from '../Utils/rest'
import { store } from '../Utils/store'

const baseUrl = 'http://localhost:3001/'
const {  usePost, useGet } = Rest(baseUrl)

const NewUser = () => {
    const data = useGet('usuarios')
    const [postData, post] = usePost('usuarios')
    const [success, setSuccess] = useState(false)
    const { state } = useContext(store)
    const valoresIniciais = {
      nome: '',
      sobrenome :'',
      email: '',
      senha: '',
      tipoUsuario: 'Usuário Padrão',
      ativo: false
    }
    const [user, setUser] = useState(valoresIniciais)
  
    const onChange = evt => {
      const { name, value } = evt.target
      const parseValue = value => {
        if (value === "true" || value === "false") {
        return value === "true" ? true : false
        } else {
        return value
        }
      }
      setUser({ ...user, [name]: parseValue(value) })
      }

    const salvar = async() => {
      await post(user)
      setUser(valoresIniciais)
      setSuccess(true)
      data.refetch()
    }
   
  
    return state.usuarioLogado.logado && state.usuarioLogado.ativo ? (
      <div>
        <h1>Novo Usuário</h1>
        <form>
          <div className='form-group'>
            <label htmlFor='nome'>Nome</label>
            <input type='text' className='form-control' id='nome' name='nome' value={user.nome} onChange={onChange} />
          

            <label htmlFor='sobrenome'>Sobrenome</label>
            <input type='text' className='form-control' id='sobrenome' name='sobrenome' value={user.sobrenome} onChange={onChange} />
        
            <label htmlFor='email'>E-mail</label>
            <input type='text' className='form-control' id='email' name='email' value={user.email} onChange={onChange} />
        
            <label htmlFor='senha'>Senha</label>
            <input type='password' className='form-control' id='senha' name='senha' value={user.senha} onChange={onChange} />
        
            <label htmlFor='situacao'>Status</label>
            <select className='form-control' id='situacao' name='ativo' value={user.ativo} onChange={onChange}>
            <option value='true' name='ativo' onChange={onChange}>Ativo</option>
            <option value='false' name='ativo' onChange={onChange} >Inativo</option>
            </select>
        
            <label htmlFor='tipo'>Tipo de usuário</label>
            <select className='form-control' id='tipo' name='tipoUsuario' value={user.tipoUsuario} onChange={onChange}>
            <option value='Administrador' name='tipoUsuario' onChange={onChange}>Administrador</option>
            <option value='Usuário Padrão' name='tipoUsuario' onChange={onChange} >Usuário Padrão</option>
            </select>
          </div>
          {success &&
            <div className='alert alert-success' role='alert'>
              Novo usuário criado com sucesso.
            </div>
          }
            <button className='btn btn-primary mr-3' type='button' onClick={salvar}>Salvar Usuário</button>
            <Link className='btn btn-primary' to='/'>Voltar</Link>
			  </form>
        
      </div>
      
    ) : (
      <div>
        <div className='alert alert-warning' role='alert'>
          Acesso Restrito. Contato com o administrador.
        </div>
        
        {!state.usuarioLogado.logado &&
        <Link className='btn btn-primary' to='/login'>Fazer Login</Link>
        }
      </div>
      )
    
  }
  export default NewUser