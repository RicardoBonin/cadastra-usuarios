import React, { useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import { store } from '../Utils/store'
import Rest from '../Utils/rest'
	
const baseUrl = 'http://localhost:3001/'
const { useGet } = Rest(baseUrl)

const Login = () => {
	const data = useGet('usuarios')
	const [email, setEmail] = useState()
  	const [senha, setSenha] = useState()
  	const { state, dispatch } = useContext(store)

	const logar = () => {
		const usuarioLogado = data.data.find(usuario => usuario.email === email ) || {}
		const usuarioExiste = Object.keys(usuarioLogado).length > 0 
    	const senhasIguais = usuarioLogado.senha === senha || ''
		const usuarioAtivo = usuarioLogado.ativo || false

		
		if(usuarioExiste && senhasIguais && usuarioAtivo){
			dispatch({ type: 'USER_LOGIN', data: usuarioLogado })
   		} else {
     		 dispatch({ type: 'LOGIN_ERROR' })
   		}
	}
	
	return (
		<div className='text-center border  mx-auto rounded w-50 p-3'>
		<h1>Faça seu login.</h1>	
			<form>
					<div className='form-group'>
						<label htmlFor='email'>E-mail</label>
						<input type='text' className='form-control w-80 mx-auto' id='email'  onChange={e => setEmail(e.target.value)} />
				
						<label htmlFor='senha'>Senha</label>
						<input type='password' className='form-control w-80 mx-auto' id='senha'   onChange={e => setSenha(e.target.value)} />
				
					</div>
						<button className='btn btn-primary w-50' type='button' onClick={logar}>Login</button>
						
			</form>

			{state.usuarioLogado.logado && 
				<Redirect to='/' />
			}
			  {state.loginError && <p>{state.loginError}</p>}
		</div>
	)
}
export default Login