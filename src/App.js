import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css'
import MostraUsuarios from './Components/MostraUsuarios'
import NewUser from './Components/NewUser'
import EditaUsuario from './Components/EditaUsuario'
import Home from './Components/Home'
import Header from './Components/Header'
import Login from './Components/Login'



function App() {
  return (
    <Switch>
      <Router>
        <Header />
        <div className='container'>
            <Route path='/' exact component={Home} />
            <Route path='/login' exact component={Login} />
            <Route path='/usuarios' exact component={MostraUsuarios} />
            <Route path='/usuarios/:id' exact component={EditaUsuario} />
            <Route path='/usuario/novo' exact component={NewUser} />
        </div>
      </Router>
    </Switch>
    
  )
}

export default App
